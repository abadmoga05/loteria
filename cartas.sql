-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 24-12-2021 a las 01:56:26
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sampledb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cartas`
--

CREATE TABLE `cartas` (
  `id` int(11) NOT NULL,
  `name` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cartas`
--

INSERT INTO `cartas` (`id`, `name`) VALUES
(1, 'El corazón'),
(2, 'El borracho'),
(3, 'El mar'),
(4, 'El cielo'),
(5, 'El sol'),
(6, 'EL ratón'),
(7, 'El burro'),
(8, 'La olla'),
(9, 'La rosa'),
(10, 'El alacrán'),
(11, 'La feria'),
(12, 'El catrín'),
(13, 'El diablo'),
(14, 'El cántaro'),
(15, 'El indio'),
(16, 'El fuego'),
(17, 'El perico'),
(18, 'La calavera'),
(19, 'El zapato'),
(20, 'La tasa'),
(21, 'El sombrero'),
(22, 'La sirena'),
(23, 'La muñeca'),
(24, 'El trompo'),
(25, 'El balero'),
(26, 'La bandera'),
(27, 'La escalera'),
(28, 'La dama'),
(29, 'La bota'),
(30, 'El negrito'),
(31, 'La estrella'),
(32, 'El mundo'),
(33, 'El nopal'),
(34, 'El músico'),
(35, 'El apache'),
(36, 'El tambor'),
(37, 'Las jaras'),
(38, 'La araña'),
(39, 'El soldado'),
(40, 'El paraguas'),
(41, 'El gallo'),
(42, 'El pescado'),
(43, 'La sandía'),
(44, 'El arpa'),
(45, 'El violín'),
(46, 'la corona'),
(47, 'La palma'),
(48, 'La garza'),
(49, 'El gorrito'),
(50, 'La botella'),
(51, 'La luna'),
(52, 'El arbol'),
(53, 'La pera'),
(54, 'El venado');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cartas`
--
ALTER TABLE `cartas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cartas`
--
ALTER TABLE `cartas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
