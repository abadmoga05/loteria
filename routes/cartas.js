
/*
 * GET users listing.
 */

exports.list = function(req, res){

  req.getConnection(function(err,connection){
       
        var query = connection.query('SELECT * FROM cartas',function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
     
            res.render('cartas',{page_title:"Cartas - Node.js",data:rows});
                
           
         });
         
         //console.log(query.sql);
    });
  
};
exports.tableros = function(req, res){

    req.getConnection(function(err,connection){
         
          var query = connection.query('SELECT casillas.idTablero,cartas.name FROM casillas,cartas WHERE casillas.idCarta = cartas.id; ',function(err,rows)
          {
              if(err)
                  console.log("Error Selecting : %s ",err );
              res.render('tableros',{page_title:"Tableros - Node.js",data:rows});
           });
           
           //console.log(query.sql);
      });
    
  };
exports.generateTableros = function(req,res){
    var query1;
    var query2;
    var randomIndex;
    var input = JSON.parse(JSON.stringify(req.body));
    req.getConnection(function (err, connection) {
        var top= Number(req.body.num_tableros);
        var i= 1;
        var x= 1;
        var tableroo=[];
        var tableronorepet = [];
        while(i <= top) {
            var datos = {id: i};
            tableroo = [];
            query1 = connection.query("INSERT INTO tableros set ?",datos, function(err, rows1){});
            x=1;
            while(x <= 16) {
                 randomIndex = getRandom();
                 if(tableroo.includes(randomIndex) == false){
                    tableroo[x-1] = randomIndex;
                    var datos2 = {idTablero: i,idCarta: randomIndex};
                    var query = connection.query("INSERT INTO casillas set ? ",datos2, function(err, rows3){
                        
                    });
                    x++;
                 }else{
                     console.log("Se elimina tarjeta "+randomIndex+" repetida en el tablero "+i);
                 }  
            }
            if(tableronorepet.includes( String(tableroo.sort( comparar )) ) == false){
                tableronorepet[i-1]= String(tableroo.sort( comparar ));
                i++;
            }else{
                connection.query("DELETE FROM casillas  WHERE idTablero ="+i, function(err, rows){});
                console.log("Se elimina tablero repetido");
            }
        }
        res.redirect("/tableros");

    });

};

exports.add = function(req, res){
  res.render('add_carta',{page_title:"Add Carta - Node.js"});
};

exports.edit = function(req, res){
    
    var id = req.params.id;
    
    req.getConnection(function(err,connection){
       
        var query = connection.query('SELECT * FROM cartas WHERE id = ?',[id],function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
     
            res.render('edit_carta',{page_title:"Edit cartas - Node.js",data:rows});
                
           
         });
         
         //console.log(query.sql);
    }); 
};

/*Save the carta*/
exports.save = function(req,res){
    
    var input = JSON.parse(JSON.stringify(req.body));
    
    req.getConnection(function (err, connection) {
        
        var data = {
            
            name    : req.body.carta
        
        };
        console.log(data);
        
        var query = connection.query("INSERT INTO cartas set ? ",data, function(err, rows)
        {
  
          if (err)
              console.log("Error inserting : %s ",err );
         
          res.redirect('/cartas');
          
        });
        
       // console.log(query.sql); get raw query
    
    });
};

exports.save_edit = function(req,res){
    
    var input = JSON.parse(JSON.stringify(req.body));
    var id = req.params.id;
    
    req.getConnection(function (err, connection) {
        
        var data = {
            
            name    : input.name,
            address : input.address,
            email   : input.email,
            phone   : input.phone 
        
        };
        
        connection.query("UPDATE cartas set ? WHERE id = ? ",[data,id], function(err, rows)
        {
  
          if (err)
              console.log("Error Updating : %s ",err );
         
          res.redirect('/cartas');
          
        });
    
    });
};


exports.delete_carta = function(req,res){
          
     var id = req.params.id;
    
     req.getConnection(function (err, connection) {
        
        connection.query("DELETE FROM carta  WHERE id = ? ",[id], function(err, rows)
        {
            
             if(err)
                 console.log("Error deleting : %s ",err );
            
             res.redirect('/cartas');
             
        });
        
     });
};
// obteniendo aleatorios en rango
function getRandom() {
    return Math.floor(Math.random() * 54)+ 1;//número de cartas en BD
}
  
// checkeando por no repetidos
function checkNotRepeat(current, validNumbers) {
    return validNumbers.includes(current)
}
//Para verificar tablero repetido
function comparar ( a, b ){ return a - b; }

