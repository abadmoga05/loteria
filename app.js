
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');

//load cartas route
var cartas = require('./routes/cartas'); 
var app = express();

var connection  = require('express-myconnection'); 
var mysql = require('mysql');

// all environments
app.set('port', process.env.PORT || 4300);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
//app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());

app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

/*------------------------------------------
    connection MySql
-------------------------------------------*/

app.use(
    
    connection(mysql,{
        
        host: 'localhost', //'localhost',
        user: 'root',
        password : '',
        port : 3306, //port mysql
        database:'sampledb'

    },'pool') //or single

);



app.get('/', routes.index);
app.get('/cartas', cartas.list);
app.get('/tableros', cartas.tableros);
app.get('/cartas/add', cartas.add);
app.post('/cartas/add', cartas.save);

app.post('/generate_tableros', cartas.generateTableros);

app.get('/cartas/delete/:id', cartas.delete_carta);
app.get('/cartas/edit/:id', cartas.edit);
app.post('/cartas/edit/:id',cartas.save_edit);


app.use(app.router);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
